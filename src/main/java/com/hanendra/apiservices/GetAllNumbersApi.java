package com.hanendra.apiservices;

import com.hanendra.RestAssuredMethodsSetup;
import com.hanendra.utils.Constants;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

public class GetAllNumbersApi {
    RestAssuredMethodsSetup restAssuredMethodsSetup=new RestAssuredMethodsSetup();

	
	 public Response GetAllUsersApiResponse(){
	        String testUrl = RestAssured.baseURI + Constants.getAllnumber;
	        return restAssuredMethodsSetup.getApi(testUrl, Constants.AuthToken);
	    }
}
