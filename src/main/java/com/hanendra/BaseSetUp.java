package com.hanendra;

import org.testng.annotations.BeforeSuite;
import com.hanendra.utils.SettingConfig;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.authentication.PreemptiveBasicAuthScheme;

public class BaseSetUp extends SettingConfig {
	
	@BeforeSuite
	public void setup(){

		// Easy way to set globally the common url-
		RestAssured.baseURI = "https://api.plivo.com";		
		
		//Can use below ways to setup for all variables 
		
		//Setting up the Environment
				if (System.getProperty("Environment") != null)
					SettingConfig.setEnvironment(System.getProperty("Environment"));
				else
					SettingConfig.setEnvironment(config_properties.getProperty("Environment"));

		///################## Setting up the URL for the suite ##################
				if (System.getProperty("url") != null)
					SettingConfig.setUrl(System.getProperty("url"));
				else
					SettingConfig.setUrl(config_properties.getProperty("url"));
	}

}
