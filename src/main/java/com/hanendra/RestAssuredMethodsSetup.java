package com.hanendra;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.response.Response;

public class RestAssuredMethodsSetup {

	   public Response response = null;

	   // Get Method with auth token 
		public Response getApi(String url, String token) {
	        response = given().authentication().oauth2(token).when().log().all().get(url).then().extract().response();
	        return response;
	    }
		
		//Post method
		  public Response postApi(String url, String requestbody, String contentType) {
		        response = given().header("Content-Type", contentType).body(requestbody).when().log().all().post(url).then()
		                .extract().response();

		        return response;
		    }
		  
}
